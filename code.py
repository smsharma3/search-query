from selenium import webdriver
from selenium.webdriver.support.ui import WebDriverWait
import requests
import dryscrape
import time
from bs4 import BeautifulSoup 
url = "http://depression.supportgroups.com/"

driver = webdriver.Firefox()
driver.get(url)

view = "//a[@id='get-past-activity']"

for i in range(1, 100):
	viewpostelement = WebDriverWait(driver, 10).until(lambda driver: driver.find_element_by_xpath(view))
	viewpostelement.click()
	time.sleep(10)

html_source = driver.page_source
soup = BeautifulSoup(html_source,'lxml')
ourdata = soup.find_all("div", {"class": "activity-item-wrapper"})
fo = open("links.txt", "wb")
for linkdata in ourdata:
	fo.write(linkdata.find("a").get("href"))
fo.close()

