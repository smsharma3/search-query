import requests
from bs4 import BeautifulSoup 
import urllib

fo = open("relationships_links.txt", "rw+")
of = open("relationships_posts.txt", "wb")
while(1):
	url = fo.readline()
	if( url is None):
		break
	# print url
	try:
		html = urllib.urlopen(url)
		soup = BeautifulSoup(html)
		ourdata = soup.find_all("span", {"class": "activity-item-body-text"})
		for linkdata in ourdata:
			of.write(linkdata.find("p").get_text())
			# print linkdata.find("p").get_text()
	except:
		continue
fo.close()
of.close()