import urllib
import re
import json
from bs4 import BeautifulSoup

htmltext = urllib.urlopen("http://relationship.supportgroups.com/ajax/sg_f/group_past_activity?max=20&before=1461816521&id=29199")
data  = json.load(htmltext)
count = 0
fo = open("relationships_links.txt", "wb")
for superentry in data['themed_output']:
	for entry in data['themed_output'][superentry]:
		html = entry
		try:
			soup = BeautifulSoup(html)
			count = count + 1
			ourdata = soup.find_all("div", {"class": "activity-item-wrapper"})
			for linkdata in ourdata:
				fo.write(linkdata.find("a").get("href")+"\n")
				# print linkdata.find("a").get("href")
		except:
			continue
fo.close()
print count;